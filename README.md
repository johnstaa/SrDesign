# SpaceX Senior Design Software Cedarville University 2021-22

This is the code repository for our Senior Design Project

## FlightController Software

Here's a short tutorial on how to start programming the Teensy with our modified dRehm flight controller

### Setup

First, install the platformIO extenstion for VSCode. There should now be a "PlatformIO" folder under Documents. Navigate to PlatformIO/Projects/ and clone this repository (GIT must be installed). There should now be a "SrDesign" folder in the "Projects" folder.

In PlatformIO, select "Open Project" and select the recently cloned "SrDesign" repository. Our code requires an extra libray to run. Go to the PlatformIO home and select "Libraies." Enter "Adafruit_VL53L0X" into the search bar and selct the first result. Add this library to the project. Now, you are ready to program the Teensy. The checkmark on the VSCode taskbar will compile the code and the arrow will compile/upload the code to the Teensy.

## Using the Radio Controller

The radio controller is an excellent tool for testing the rocket. Below are some instructions on how to use the controller to dynamically change the gain values for the PID angle controller.

### Controlling Gain Values

On the top of the controller are four switches and two knobs. The switches are labeled SWA, SWB, SWC, and SWD. The knobs are VRA and VRB. We utilize these controls as follows:

SWA -> Pitch/Roll: This switch decides whether Pitch or Roll is being modified.
SWB -> Not Used: I think this switch is broken.
VRA -> Gain contorl: This knob controls the gain.
VRB -> Not Used: Could be used in the future.
SWC -> P/D/I Selection: Contorls which PID gain is being changed by the knob.
SWD -> Edit/Save: Turning this switch off (down) will stop any values from changing.

Here's an example on how to use these controls. Start with all the switches in the up postion. This would be controlling the pitch's P gain. Find the desired gain with the VRA and then flip SWD down to stop the gain from changing. Change the ptich/roll and PID selection swith as desired and flip SWD back on to start changing that gain. 

## References

This project is based off of the design by Nick Rehm shown in this link https://github.com/nickrehm/dRehmFlight
