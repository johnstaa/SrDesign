# -*- coding: utf-8 -*-

from cgitb import text
from unittest import TextTestResult
import serial, time, os,sys,glob,serial.tools.list_ports
from datetime import datetime,date,timedelta

'''
Important Usage Vars, Change these to make it
work for your computer
'''
# Make this the correct comport on you computer
#COMPORT = "COM4" # School Computer
# COMPORT = "/dev/tty.usbmodem101228701" # Aarons Laptop  `ls /dev/tty.*`
# Optionally tune your baudrate
BAUDRATE = 9600
# File Name
fileName=sys.argv[1]
ds = sys.argv[2]
eeprom = sys.argv[3]
''' 
Functionality Description:
    This program is intended to allow
    us to read data through the serial
    USB connection to the teensy. It then
    writes the value from the data into a
    txt file to be parsed, graphed, and
    analyzed.
'''
def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    # for windows
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    # for linux
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    # for mac
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

def getConnectedRead():
    #print('gotintocollect')
    try:
        while(1):
            try:
                #print('tryingto connect to serial')
                ser = serial.Serial(COMPORT, BAUDRATE, timeout=0)
                connected = 1
                print("Connected!")
                break
            except:
                #print('failed to connect to serial')
                time.sleep(1)
                pass 
    except KeyboardInterrupt:
        print("Program aborted")
        return
    
    try:
        dt = datetime.now().strftime("%Y-%m-%d%H-%M-%S-%f")
        print(dt)
        filepath = os.getcwd() + "/" + 'Flight' + fileName +".txt"
        print(filepath)
        text_file = open(filepath, "w")
        i = 0
        while(connected == 1):
            #print(len(ser.inWaiting()))

            if(ser.in_waiting > 0):
                i = 0
                line = ser.readline()
                if (line != ""):
                    # write to file
                    line = line.decode()
                    # print(line)
                    if(line.startswith('End of data') and eeprom == 'True'):
                        ser.close()
                        text_file.close()
                        break
                    text_file.write(line)
                    if(ds == 'False'):
                        if(line.startswith('RAN')):
                            print(line)
        '''else:
                time.sleep(1)
                i+=1
                if(ser.in_waiting == 0):
                    if(i == 10):
                        print("Took too long")
                        text_file.close()
                        ser.close()
                        return'''
                                 
    except KeyboardInterrupt:
        print("Program aborted")
        ser.close()
        text_file.close()
        return
    except:
        print("Connection failed")
        ser.close()
        text_file.close()
        if(eeprom == 'True'):
            raise Exception("Failed Connection, incomplete transfer")
        return  
# IF IT FAILS THE FIRST TIME, Make this the correct comport on you computer, it should list all open ports even if it selects the wrong one
# COMPORT = "COM4" # School Computer
# COMPORT = "/dev/tty.usbmodem101228701" # Aarons Laptop  `ls /dev/tty.*`
while(1):
    comlist = serial_ports()
    if(len(comlist)!=0):
        print(comlist)
        COMPORT=comlist[-1]
        break
getConnectedRead()
filepath = os.getcwd() + "/" + 'Flight' + fileName +".txt"
with open(filepath,'r') as f:
	file = f.readlines()
numerrors = 0
for x in file:
	data = x.split(',')
	if(len(data)!=25):
	    numerrors+=1
# print(numerrors)
# if(numerrors>=5):
	# raise Exception("Too many serial transmission errors, retransmit")