#!/bin/usr/env bash

for n in {200..374};
do
    python3 InsertData.py $n
    python3 CreateCSV.py $n
    python3 gradeFlight.py $n
    echo $n
done
