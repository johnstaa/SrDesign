#!/bin/bash

# This script will install dependencies needed for sr design

sudo apt install python3-pip
pip install datapane
pip install matplotlib
pip install numpy
pip install pandas
pip install Pillow
pip install plotly
pip install pynmea2
pip install pyserial
pip install pysqlite3
pip install xlsxwriter