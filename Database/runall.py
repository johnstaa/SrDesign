#   File Name: runally.py 
#   Author: Aaron Johnston
#   Email: ajohnsto@cedarville.edu
#   Date Created: November 18th, 2021
#   Purpose: This file is intended to be a master control file to 
#            run all database collection and insertion scripts in an automated fashion.
#
#   Cedarville University.
#   SpaceX Model Rocket Senior Design Team
#   CEO - Timothy Kohl
#   CFO - Anthony Chaffey
#   CTO - Jack Williamson
#   CSO - Aaron Johnston
#

import os
import shutil
import sys
from subprocess import check_call, CalledProcessError
import sqlite3
import pandas as pd
import subprocess



# ds stands for the -ds (dont suppress) flag, this is designed so the user has an option to print the gains
ds = True
# Eeprom is whether the data is coming through on the USB or the eeprom
eeprom = False
if(len(sys.argv) > 1):
    if(sys.argv[1] == '-ds'):
        ds = False
    elif(sys.argv[1] == '-eep'):
        eeprom=True


# Open database and create a cursor
con = sqlite3.connect('./Rocket.db')
db = con.cursor()
db.execute('PRAGMA foreign_keys = ON') # This is required as default sqlite turns off FK's

# Grab Highest Rocket Flight ID
maxRF = pd.read_sql('Select MAX(Flight_ID) from Rocket_Flight',con)
# Add new data in at the next available slot
rf1 = -1
rf1 = int (maxRF.values) + 1
try:
     #   Run Jacks PySerial Script
     print('Collecting Serial Data . . .')
     os.chdir('../SerialTestPython')
     check_call(['python','collectData.py', f'{rf1}',f'{ds}',f'{eeprom}']) 
     print('Finished Collecting Data . . .')
     # Move Data to correct Location
     print('Attempting to move data file to directory . . .') 
     # Remove flight data file if text already exsists
     subprocess.call(['rm', f'../Database/Data/Flights/Flight{rf1}.txt']) 
     shutil.move(f'Flight{rf1}.txt', '../Database/Data/Flights')
     os.chdir('../Database/Insertion')
     print('Inserting Data . . . ')
     loc = input("Flight Location: ")
     inp = input("Flight Description: ")
     score = ", Score: "
     score += input("How would you like to score the Flight? (0-10): ")
     check_call(['python','insertData.py', f'{rf1}',f'{eeprom}', f'{inp+score}',f'{loc}'])
    #  print('\033[92m Creating CSV File. . .')
    #  check_call(['python','createCSV.py' ,f'{rf1}'])
     print('Scoring Flight')
     check_call(['python', 'gradeFlight.py', f'{rf1}'])   # Comment this line out if it is taking forever to run
     # Swap directories to call create report
     os.chdir("../Reporting")
     print('Creating report data . . .')
     check_call(['python','createReport.py',f'{rf1}'])
except CalledProcessError as e:
    print(f'The Following error occurred in Database/Runall.py: {e}')